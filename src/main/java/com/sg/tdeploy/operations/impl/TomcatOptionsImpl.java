package com.sg.tdeploy.operations.impl;

import com.google.inject.Singleton;
import com.sg.tdeploy.operations.ITomcatOptions;
import org.apache.commons.io.FileUtils;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.stream.IntStream;
import java.util.stream.Stream;

/**
 * Created by: Hleb_Shauchenka<br>
 */
@Singleton
public class TomcatOptionsImpl implements ITomcatOptions {

    private String separator = File.separator;
    private String binary = separator + "bin";
    private String webapps = separator + "webapps";
    private String serverxml = separator + "conf" + separator + "server.xml";

    private String connectorStr = "Connector";
    private String protocolStr = "protocol";
    private String httpStr = "HTTP/1.1";
    private String portStr = "port";

    private List<String> cmdStartArgs = Arrays.asList("cmd", "/c", "startup.bat");
    private List<String> cmdStopArgs = Arrays.asList("cmd", "/c", "shutdown.bat");

    private DocumentBuilderFactory dbFactory;
    private DocumentBuilder dBuilder;
    private Document doc;
    private TransformerFactory transformerFactory;
    private Transformer transformer;
    private DOMSource source;
    private StreamResult result;

    @Override
    public void startTomcat(String tomPath) throws IOException {

        String tomBin = tomPath + binary;

        File dir = new File(tomBin);
        ProcessBuilder processBuilder = new ProcessBuilder(cmdStartArgs);
        processBuilder.directory(dir);
        Process process = processBuilder.start();
    }

    @Override
    public void stopTomcat(String tomPath) throws IOException {

        String tomBin = tomPath + binary;

        File dir = new File(tomBin);
        ProcessBuilder processBuilder = new ProcessBuilder(cmdStopArgs);
        processBuilder.directory(dir);
        Process process = processBuilder.start();
    }

    @Override
    public void deploy(String tomPath, String warPath) throws IOException {

        File source = new File(warPath);
        String warName = source.getName();
        File dest = new File(tomPath + webapps + separator + warName);

        FileUtils.copyFile(source, dest);
    }

    @Override
    public boolean checkIsTomcatDirValid(String tomPath){

        File dir = new File(tomPath);
        if (dir.list() == null) return false;
        if (!Arrays.asList(dir.list()).contains("bin")) return false;
        File binDir = new File(tomPath + binary);
        if (!Arrays.asList(binDir.list()).contains("startup.bat")) return false;

        return true;
    }

    @Override
    public void setTomcatHttpPortNumber(String tomPath, int number) throws IOException {

        File xmlFile = new File(tomPath + serverxml);

        if (!isDomInitialized(dbFactory, dBuilder, doc, transformerFactory, transformer, source, result)){
            try{
                dbFactory = DocumentBuilderFactory.newInstance();
                dBuilder = dbFactory.newDocumentBuilder();
                doc = dBuilder.parse(xmlFile);

                transformerFactory = TransformerFactory.newInstance();
                transformer = transformerFactory.newTransformer();
                source = new DOMSource(doc);
                result = new StreamResult(xmlFile);
            }
            catch (TransformerConfigurationException | SAXException | ParserConfigurationException e) {
                throw new IOException(e.getMessage(), e);
            }
        }

        NodeList nodeList = doc.getElementsByTagName(connectorStr);
        Stream<Node> nodeStream = IntStream.range(0, nodeList.getLength()).mapToObj(nodeList::item);
        Optional<Node> httpConnector = nodeStream.filter(node -> node.getAttributes()
                                                                     .getNamedItem(protocolStr).getNodeValue()
                                                                     .equals(httpStr)).findFirst();
        httpConnector.ifPresent(node -> node.getAttributes().getNamedItem(portStr).setNodeValue(String.valueOf(number)));

        try{
            transformer.transform(source, result);
        }
        catch (TransformerException e) {
            throw new IOException(e.getMessage(), e);
        }

    }


    private boolean isDomInitialized(DocumentBuilderFactory dbf, DocumentBuilder dBuilder, Document doc,
                                     TransformerFactory tf, Transformer transformer, DOMSource domSource,
                                     StreamResult streamResult){

        if (dbf == null || dBuilder == null || doc == null || tf == null
            || transformer == null || domSource == null || streamResult == null)
            return false;

        return true;
    }


}
