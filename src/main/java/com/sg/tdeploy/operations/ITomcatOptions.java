package com.sg.tdeploy.operations;

import java.io.IOException;

/**
 * Created by: Hleb_Shauchenka<br>
 */
public interface ITomcatOptions {

    void startTomcat(String tomPath) throws IOException;
    void stopTomcat(String tomPath) throws IOException;
    void deploy(String tomPath, String warPath) throws IOException;
    boolean checkIsTomcatDirValid(String tomPath);
    void setTomcatHttpPortNumber(String tomPath, int number) throws IOException;

}
