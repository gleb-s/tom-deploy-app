package com.sg.tdeploy;

import com.google.inject.Guice;
import com.google.inject.Injector;
import com.sg.tdeploy.di.AppInjectorConfig;
import com.sg.tdeploy.di.GuiceControllerFactory;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Stage;

public class AppRunner extends Application {

    @Override
    public void start(Stage primaryStage) throws Exception{
        Injector injector = Guice.createInjector(new AppInjectorConfig());
        FXMLLoader loader = new FXMLLoader(getClass().getClassLoader().getResource("sample.fxml"));
        loader.setControllerFactory(new GuiceControllerFactory(injector));
        Parent root = loader.load();
        primaryStage.setTitle("Tomcat deployer app");
        primaryStage.setScene(new Scene(root));
        primaryStage.setResizable(false);
        primaryStage.getIcons().add(new Image("/ico/cat.png"));
        primaryStage.show();
    }


    public static void main(String[] args) {
        launch(args);
    }
}
