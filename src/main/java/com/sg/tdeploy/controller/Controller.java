package com.sg.tdeploy.controller;

import com.google.inject.Inject;
import com.sg.tdeploy.operations.ITomcatOptions;
import com.sg.tdeploy.util.Alerts;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.stage.DirectoryChooser;
import javafx.stage.FileChooser;

import java.io.File;
import java.io.IOException;

public class Controller {

    @FXML private TextField inputWar;
    @FXML private TextField inputTom;
    @FXML private TextField inputPort;
    @FXML private Button buttonStart;
    @FXML private Button buttonStop;
    @FXML private Button buttonDeploy;
    @FXML private Label labelInfo;
    private DirectoryChooser directoryChooser = new DirectoryChooser();
    private FileChooser fileChooser = new FileChooser();

    @Inject private ITomcatOptions tomcatOptions;

    @FXML
    public void initialize() {
        buttonStart.setTooltip(new Tooltip("Runs \"startup.bat\" file"));
        buttonStop.setTooltip(new Tooltip("Runs \"shutdown.bat\" file"));

        directoryChooser.setTitle("Choose path to your Tomcat folder");
        fileChooser.setTitle("Choose path to your .war");
        FileChooser.ExtensionFilter warFilter =
                new FileChooser.ExtensionFilter("WAR archives (*.war)", "*.war");
        fileChooser.getExtensionFilters().add(warFilter);

        inputPort.textProperty().addListener((observable, oldValue, newValue) -> {
            if (!newValue.matches("\\d*")) {
                inputPort.setText(newValue.replaceAll("[^\\d]", ""));
            }
        });
    }

    public void findWarPath() {
        labelInfo.setText("Info:");

        File war = fileChooser.showOpenDialog(null);
        if (war != null) {
            labelInfo.setText("File selected: " + war.getName());
            inputWar.setText(war.getAbsolutePath());
        }
        else {
            labelInfo.setText("File selection cancelled.");
        }
    }

    public void findTomPath() {
        labelInfo.setText("Info:");

        File tomDir = directoryChooser.showDialog(null);
        if (tomDir != null) {
            if (tomcatOptions.checkIsTomcatDirValid(tomDir.getAbsolutePath())){
                labelInfo.setText("This is valid Tomcat directory");
                inputTom.setText(tomDir.getAbsolutePath());
            } else {
                labelInfo.setText("This is invalid Tomcat directory");
            }
        }
        else {
            labelInfo.setText("Directory selection cancelled.");
        }
    }

    public void startTomcat()  {
        labelInfo.setText("Info:");

        try{
            if (inputTom.getText() != null && !inputTom.getText().equals("")){
                if (inputPort.getText() != null && !inputPort.getText().equals(""))
                    tomcatOptions.setTomcatHttpPortNumber(inputTom.getText(), Integer.parseInt(inputPort.getText()));
                tomcatOptions.startTomcat(inputTom.getText());
            }
            else labelInfo.setText("Choose Tomcat path");
        }
        catch (IOException e) {
            Alerts.showErrorDialog(e);
        }

    }

    public void stopTomcat() {
        labelInfo.setText("Info:");

        try {
            if (inputTom.getText() != null && !inputTom.getText().equals(""))
                tomcatOptions.stopTomcat(inputTom.getText());
            else labelInfo.setText("Choose Tomcat path");
        }
        catch (IOException e) {
            Alerts.showErrorDialog(e);
        }

    }

    public void deploy() {
        labelInfo.setText("Info:");

        try {
            if (inputTom.getText() != null && !inputTom.getText().equals("")
                    && inputWar.getText() != null && !inputWar.getText().equals("")) {
                tomcatOptions.deploy(inputTom.getText(), inputWar.getText());
                labelInfo.setText("Copied war to webapps!");
            }
            else labelInfo.setText("Choose Tomcat path");
        }
        catch (IOException e) {
            Alerts.showErrorDialog(e);
        }
    }
}
