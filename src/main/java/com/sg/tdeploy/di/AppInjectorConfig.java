package com.sg.tdeploy.di;

import com.google.inject.AbstractModule;
import com.sg.tdeploy.operations.ITomcatOptions;
import com.sg.tdeploy.operations.impl.TomcatOptionsImpl;

/**
 * Created by: Hleb_Shauchenka<br>
 *
 *     Guice dependency mapping configuration
 */
public class AppInjectorConfig extends AbstractModule {

    @Override
    protected void configure() {
        bind(ITomcatOptions.class).to(TomcatOptionsImpl.class);
    }
}
