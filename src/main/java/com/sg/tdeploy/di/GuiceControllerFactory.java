package com.sg.tdeploy.di;

import com.google.inject.Injector;
import javafx.util.Callback;

/**
 * Created by: Hleb_Shauchenka<br>
 *
 *     Used to get controllers via Guice DI engine.
 */
public class GuiceControllerFactory implements Callback<Class<?>, Object> {

    private final Injector injector;

    public GuiceControllerFactory(Injector anInjector) {
        injector = anInjector;
    }

    @Override
    public Object call(Class<?> aClass) {
        return injector.getInstance(aClass);
    }
}
